use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Quad {
    pub top_left_corner: [f32; 2],
    pub width: f32,
    pub height: f32,
    pub vertices: [f32; 8],
    pub edges: [f32; 4],
}

impl Quad {
    pub fn new(top_left_corner: [f32; 2], width: f32, height: f32) -> Self {
        let corner_x = -1.0 + top_left_corner[0] / 640.0;
        let corner_y = 1.0 - top_left_corner[1] / 360.0;
        let vertices: [f32; 8] = [
            corner_x, corner_y,                                  // Top left Vertex
            corner_x + width / 640.0, corner_y,                  // Top right Vertex
            corner_x, corner_y - height / 360.0,                 // Bottom left Vertex
            corner_x + width / 640.0, corner_y - height / 360.0, // Bottom right Vertex
        ];
        let edges: [f32; 4] = [
            corner_y - height / 360.0,  // Bottom edge
            corner_x,                   // Left edge
            corner_x + width / 640.0,   // Right edge
            corner_y,                   // Top edge
        ];
        //println!("{:?}", edges);
        Self {
            top_left_corner,
            width,
            height,
            vertices,
            edges,
        }
    }
    /*
    pub fn from_json(json: &str) -> Result<Self, serde_json::Error> {
        serde_json::from_str(json)
    }

    pub fn to_json(&self) -> Result<String, serde_json::Error> {
        serde_json::to_string(self)
    }
    */
}
