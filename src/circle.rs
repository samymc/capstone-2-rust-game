pub struct Circle {
    pub position: [f32; 2],
    pub size: f32,
    pub vertices: [f32; 84],
    pub edge_offsets: [f32; 2],
}

impl Circle {
    pub fn new(position: [f32; 2], size: f32) -> Self {
        let vertices: [f32; 84] = [
            0.0, 0.0, // Center
            0.0, size / 720.0, // Top Vertex
            // Top Right Quadrant
            0.1564 * size / 1280.0, 0.9877 * size / 720.0,
            0.3090 * size / 1280.0, 0.9511 * size / 720.0,
            0.4540 * size / 1280.0, 0.8910 * size / 720.0,
            0.5878 * size / 1280.0, 0.8090 * size / 720.0,
            0.7071 * size / 1280.0, 0.7071 * size / 720.0,
            0.8090 * size / 1280.0, 0.5878 * size / 720.0,
            0.8910 * size / 1280.0, 0.4540 * size / 720.0,
            0.9511 * size / 1280.0, 0.3090 * size / 720.0,
            0.9877 * size / 1280.0, 0.1564 * size / 720.0,
            // End Top Right Quadrant
            size / 1280.0, 0.0, // Right Vertex
            // Bottom Right Quadrant
            0.9877 * size / 1280.0, -0.1564 * size / 720.0,
            0.9511 * size / 1280.0, -0.3090 * size / 720.0,
            0.8910 * size / 1280.0, -0.4540 * size / 720.0,
            0.8090 * size / 1280.0, -0.5878 * size / 720.0,
            0.7071 * size / 1280.0, -0.7071 * size / 720.0,
            0.5878 * size / 1280.0, -0.8090 * size / 720.0,
            0.4540 * size / 1280.0, -0.8910 * size / 720.0,
            0.3090 * size / 1280.0, -0.9511 * size / 720.0,
            0.1564 * size / 1280.0, -0.9877 * size / 720.0,
            // End Bottom Right Quadrant
            0.0, -size / 720.0, // Bottom Vertex
            // Bottom Left Quadrant
            -0.1564 * size / 1280.0, -0.9877 * size / 720.0,
            -0.3090 * size / 1280.0, -0.9511 * size / 720.0,
            -0.4540 * size / 1280.0, -0.8910 * size / 720.0,
            -0.5878 * size / 1280.0, -0.8090 * size / 720.0,
            -0.7071 * size / 1280.0, -0.7071 * size / 720.0,
            -0.8090 * size / 1280.0, -0.5878 * size / 720.0,
            -0.8910 * size / 1280.0, -0.4540 * size / 720.0,
            -0.9511 * size / 1280.0, -0.3090 * size / 720.0,
            -0.9877 * size / 1280.0, -0.1564 * size / 720.0,
            // End Bottom Left Quadrant
            -size / 1280.0, 0.0, // Left Vertex
            // Top Left Quadrant
            -0.9877 * size / 1280.0, 0.1564 * size / 720.0,
            -0.9511 * size / 1280.0, 0.3090 * size / 720.0,
            -0.8910 * size / 1280.0, 0.4540 * size / 720.0,
            -0.8090 * size / 1280.0, 0.5878 * size / 720.0,
            -0.7071 * size / 1280.0, 0.7071 * size / 720.0,
            -0.5878 * size / 1280.0, 0.8090 * size / 720.0,
            -0.4540 * size / 1280.0, 0.8910 * size / 720.0,
            -0.3090 * size / 1280.0, 0.9511 * size / 720.0,
            -0.1564 * size / 1280.0, 0.9877 * size / 720.0,
            // End Top Left Quadrant
            0.0, size / 720.0, // Top Vertex
        ];
        let edge_offsets: [f32; 2] = [
            size / 1280.0, // Horizontal Offset
            size / 720.0,  // Vertical Offset
        ];

        Self {
            position,
            size,
            vertices,
            edge_offsets,
        }
    }
}