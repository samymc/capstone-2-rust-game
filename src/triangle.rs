use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Triangle {
    pub position: [f32; 2],
    pub size: f32,
    pub vertices: [f32; 6],
    pub edge_offsets: [f32; 2],
}

impl Triangle {
    pub fn new(position: [f32; 2], size: f32) -> Self {
        let new_position = [-1.0 + position[0] / 640.0, 1.0 - position[1] / 360.0];
        let vertices: [f32; 6] = [
            -size / 1280.0, -size / 720.0, // Bottom left Vertex
            0.0, size / 720.0,             // Top Vertex
            size / 1280.0, -size / 720.0,  // Bottom right Vertex
        ];
        let edge_offsets: [f32; 2] = [
            size / 1280.0, // Horizontal offset
            size / 720.0,  // Vertical offset
        ];

        Self {
            position: new_position,
            size,
            vertices,
            edge_offsets,
        }
    }
    /*
    pub fn from_json(json: &str) -> Result<Self, serde_json::Error> {
        serde_json::from_str(json)
    }

    pub fn to_json(&self) -> Result<String, serde_json::Error> {
        serde_json::to_string(self)
    }
    */
}
