#![allow(temporary_cstring_as_ptr)]
mod triangle;
mod circle;
mod quad;

use triangle::Triangle;
use circle::Circle;
use quad::Quad;

use std::ffi::CString;
use std::time::Instant;
use std::collections::HashSet;

use glutin::{
    event::{ElementState, Event, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
    ContextBuilder,
};

use nalgebra_glm as glm;

// Create a shader program
fn create_shader_program(vertex_shader_src: CString, r: f32, g: f32, b: f32, alpha: f32) -> u32 {
    let mut _program: u32 = 0;
    let fragment_shader_src = create_fragment_shader(r, g, b, alpha);
    unsafe {
        _program = gl::CreateProgram();
        let vertex_shader = gl::CreateShader(gl::VERTEX_SHADER);
        gl::ShaderSource(vertex_shader, 1, &vertex_shader_src.as_ptr(), std::ptr::null());
        gl::CompileShader(vertex_shader);

        let fragment_shader = gl::CreateShader(gl::FRAGMENT_SHADER);
        gl::ShaderSource(fragment_shader, 1, &fragment_shader_src.as_ptr(), std::ptr::null());
        gl::CompileShader(fragment_shader);

        
        gl::AttachShader(_program, vertex_shader);
        gl::AttachShader(_program, fragment_shader);
        gl::LinkProgram(_program);

        gl::DeleteShader(vertex_shader);
        gl::DeleteShader(fragment_shader);
    };
    _program
}

// Create a fragment shader with given rgb and alpha values, valid rgb are from 0 to 255
fn create_fragment_shader(r: f32, g: f32, b: f32, alpha: f32) -> CString {
    CString::new(format!(r#"
    #version 330 core
    out vec4 FragColor;

    void main() {{
        FragColor = vec4({}, {}, {}, {});
    }}
    "#, r / 255.0, g / 255.0, b / 255.0, alpha)).unwrap()
}

// initialize vao and vbo of an object, if dynamic is true set the buffer data as dynamic; otherwise set it to static
fn initialize_object_vos(vertices: &[f32], dynamic: bool, vaos: &mut Vec<u32>, vbos: &mut Vec<u32>) -> () {
    let mut vao: u32 = 0;
    let mut vbo: u32 = 0;
    unsafe {
        let draw_type = if dynamic {gl::DYNAMIC_DRAW} else {gl::STATIC_DRAW};
        gl::GenVertexArrays(1, &mut vao);
        gl::GenBuffers(1, &mut vbo);
        gl::BindVertexArray(vao);
        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            (vertices.len() * std::mem::size_of::<f32>()) as isize,
            vertices.as_ptr() as *const _,
            draw_type,
        );
        gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE, 0, std::ptr::null());
        gl::EnableVertexAttribArray(0);
    }
    vaos.push(vao);
    vbos.push(vbo);
}

fn create_enemy(position: [f32; 2], size: f32) -> Triangle {
    let enemy = Triangle::new(position, size);
    enemy
}

fn draw_triangle() -> () {
    unsafe { gl::DrawArrays(gl::TRIANGLES, 0, 3); }
}

fn draw_rectangle() -> () {
    unsafe { gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4); }
}

// Not actually a circle, just close
fn draw_circle() -> () {
    unsafe { gl::DrawArrays(gl::TRIANGLE_FAN, 0, 42); }
}

fn pixel_position_to_logical_position(x: f32, y: f32) -> [f32; 2] {
    [-1.0 + x / 640.0, 1.0 - y / 360.0]
}

fn get_unit_vector(vector: [f32;2]) -> [f32;2] {
    let magnitude: f32 = (vector[0]*vector[0] + vector[1]*vector[1]).sqrt();
    return [vector[0] / magnitude, vector[1] / magnitude];
}

fn handle_player_movement(player: &mut Triangle, delta_seconds: f32, x_velocity: &mut f32, friction: f32, max_speed: f32, y_velocity: &mut f32, gravity: f32, jumping: &mut bool, collision_bounds: &Vec<[f32; 4]>) -> () {
    // Handle movement on y axis
    let mut collided: bool = false;
    let next_y_position: f32 = (*player).position[1] + (*y_velocity - gravity * delta_seconds) * delta_seconds;
    if (*player).position[1] - (*player).edge_offsets[1] > -1.0 || *y_velocity >= 0.0 { // Moving upwards
        for boundary_set in collision_bounds {
            if (*player).position[0] - (*player).edge_offsets[0] < boundary_set[2] && (*player).position[0] + (*player).edge_offsets[0] > boundary_set[1] {
                if (*player).position[1] - (*player).edge_offsets[1] >= boundary_set[3] {
                    if next_y_position - (*player).edge_offsets[1] < boundary_set[3] {
                        //println!("Next position crosses through upper boundary");
                        collided = true;
                        (*player).position[1] = boundary_set[3] + (*player).edge_offsets[1];
                        *y_velocity = 0.0;
                        *jumping = false;
                    }
                }
                else if (*player).position[1] + (*player).edge_offsets[1] < boundary_set[0] {
                    if next_y_position + (*player).edge_offsets[1] > boundary_set[0] {
                        //println!("Next position crosses through lower boundary");
                        collided = true;
                        (*player).position[1] = boundary_set[0] - (*player).edge_offsets[1];
                        *y_velocity = 0.0;
                    }
                }
            }
            
        }
        if (*player).position[1] + (*player).edge_offsets[1] <= 1.0 && next_y_position + (*player).edge_offsets[1] > 1.0 {
            collided = true;
            (*player).position[1] = 1.0 - (*player).edge_offsets[1];
            *y_velocity = 0.0;
        }
        if !collided {
            *y_velocity -= gravity * delta_seconds;
            (*player).position[1] += *y_velocity * delta_seconds;
        }
            
    } else { // Colliding with the ground
        (*player).position[1] = -1.0 + (*player).edge_offsets[1];
        *y_velocity = 0.0;
        *jumping = false;
    }
    
    // Handle movement on x axis
    collided = false;
    if *x_velocity > 0.01 { // If velocity is positive with margin
        *x_velocity -= friction * delta_seconds;
        if *x_velocity > max_speed { *x_velocity = max_speed; } // Cap speed
        let next_x_position: f32 = (*player).position[0] + *x_velocity * delta_seconds;
        if (*player).position[0] + (*player).edge_offsets[0] < 1.0 { // Check for out of bounds
            for boundary_set in collision_bounds {
                if (*player).position[1] - (*player).edge_offsets[1] < boundary_set[3] && (*player).position[1] + (*player).edge_offsets[1] > boundary_set[0] {
                    if (*player).position[0] + (*player).edge_offsets[0] <= boundary_set[1] {
                        if next_x_position + (*player).edge_offsets[0] > boundary_set[1] {
                            collided = true;
                            (*player).position[0] = boundary_set[1] - (*player).edge_offsets[0];
                            *x_velocity = 0.0;
                        }
                    }
                }
            }
            if !collided {(*player).position[0] += *x_velocity * delta_seconds }
            
        } else {
            (*player).position[0] = 1.0 - (*player).edge_offsets[0];
        }
    } else if *x_velocity < -0.01 { // If velocity is negative with margin
        *x_velocity += friction * delta_seconds;
        if *x_velocity < -max_speed { *x_velocity = -max_speed; } // Cap speed
        let next_x_position: f32 = (*player).position[0] + *x_velocity * delta_seconds;
        if (*player).position[0] - (*player).edge_offsets[0] > -1.0 { // Check for out of bounds
            for boundary_set in collision_bounds {
                if (*player).position[1] - (*player).edge_offsets[1] < boundary_set[3] && (*player).position[1] + (*player).edge_offsets[1] > boundary_set[0] {
                    if (*player).position[0] - (*player).edge_offsets[0] >= boundary_set[2] {
                        if next_x_position - (*player).edge_offsets[0] < boundary_set[2] {
                            collided = true;
                            (*player).position[0] = boundary_set[2] + (*player).edge_offsets[0];
                            *x_velocity = 0.0;
                        }
                    }
                }
            }
            if !collided {(*player).position[0] += *x_velocity * delta_seconds }
        } else {
            (*player).position[0] = -1.0 + (*player).edge_offsets[0];
        }
    } else { *x_velocity = 0.0; } // If velocity is inside margin, ensures player doesnt wiggle when not moving
}

fn handle_projectile_movement(projectile: &mut Circle, unit_direction: &[f32;2], speed: f32, delta_seconds: f32, collision_bounds: &Vec<[f32; 4]>) -> bool {
    //println!("Starting handle_projectile_movement with start position of {:?} and unit_direction of {:?}", (*projectile).position, unit_direction);
    let next_y_position: f32 = (*projectile).position[1] + unit_direction[1] * speed * delta_seconds;
    let mut collided_y: bool = false;
    for boundary_set in collision_bounds {
        if (*projectile).position[0] - (*projectile).edge_offsets[0] < boundary_set[2] && (*projectile).position[0] + (*projectile).edge_offsets[0] > boundary_set[1] {
            if (*projectile).position[1] - (*projectile).edge_offsets[1] >= boundary_set[3] {
                if next_y_position - (*projectile).edge_offsets[1] < boundary_set[3] {
                    collided_y = true;
                    (*projectile).position[1] = boundary_set[3] + (*projectile).edge_offsets[1];
                }
            }
            else if (*projectile).position[1] + (*projectile).edge_offsets[1] < boundary_set[0] {
                if next_y_position + (*projectile).edge_offsets[1] > boundary_set[0] {
                    collided_y = true;
                    (*projectile).position[1] = boundary_set[0] - (*projectile).edge_offsets[1];
                }
            }
        }
        
    }
    if next_y_position > 1.0 {
        collided_y = true;
        (*projectile).position[1] = 1.0 - (*projectile).edge_offsets[1];
    }
    else if next_y_position < -1.0 {
        collided_y = true;
        (*projectile).position[1] = -1.0 + (*projectile).edge_offsets[1];
    }
    if !collided_y {
        (*projectile).position[1] = next_y_position;
    }

    let mut collided_x = false;
    let next_x_position: f32 = (*projectile).position[0] + unit_direction[0] * speed * delta_seconds;
    for boundary_set in collision_bounds {
        if (*projectile).position[1] - (*projectile).edge_offsets[1] < boundary_set[3] && (*projectile).position[1] + (*projectile).edge_offsets[1] > boundary_set[0] {
            if unit_direction[0] < 0.0 && (*projectile).position[0] - (*projectile).edge_offsets[0] >= boundary_set[2] {
                if next_x_position - (*projectile).edge_offsets[0] < boundary_set[2] {
                    collided_x = true;
                    (*projectile).position[0] = boundary_set[2] + (*projectile).edge_offsets[0];
                }
            }
            else if unit_direction[0] > 0.0 && (*projectile).position[0] + (*projectile).edge_offsets[0] <= boundary_set[1] {
                if next_x_position + (*projectile).edge_offsets[0] > boundary_set[1] {
                    collided_x = true;
                    (*projectile).position[0] = boundary_set[1] - (*projectile).edge_offsets[0];
                }
            }
        }
    }
    if next_x_position > 1.0 {
        collided_x = true;
        (*projectile).position[0] = 1.0 - (*projectile).edge_offsets[0];
    }
    else if next_x_position < -1.0 {
        collided_x = true;
        (*projectile).position[0] = -1.0 + (*projectile).edge_offsets[0];
    }

    if !collided_x {
        (*projectile).position[0] = next_x_position;
    }
    
    //println!("Ending handle_projectile_movement with end position of {:?}", (*projectile).position);
    return collided_y || collided_x;
}

fn main() {
    // Set up window
    let pixel_width: f32 = 1280.0;
    let pixel_height: f32 = 720.0;
    let event_loop = EventLoop::new();
    let window_builder = WindowBuilder::new()
        .with_title("Capstone 2 Rust Game")
        .with_inner_size(glutin::dpi::LogicalSize::new(pixel_width, pixel_height))
        .with_fullscreen(Some(glutin::window::Fullscreen::Borderless(None)));
    let gl_window = ContextBuilder::new()
        .with_vsync(true)
        .build_windowed(window_builder, &event_loop)
        .unwrap();
    let gl_window = unsafe { gl_window.make_current().unwrap() };
    let monitor: glutin::monitor::MonitorHandle = glutin::window::Window::current_monitor(gl_window.window()).unwrap();
    let monitor_size: glutin::dpi::PhysicalSize<u32> = monitor.size();
    let monitor_height_ratio: f32 = 720.0 / monitor_size.height as f32;
    let monitor_width_ratio: f32 = 1280.0 / monitor_size.width as f32;
    println!("Monitor width ratio = {0}, Monitor height ratio = {1}", monitor_width_ratio, monitor_height_ratio);
    gl::load_with(|s| gl_window.get_proc_address(s) as *const _);

    let mut game_stage: i32 = 0;
    let max_stage: i32 = 2;


    // Create a player triangle
    let mut player: Triangle = Triangle::new([160.0, 700.0], 30.0);

    let player_spawn_x = -0.9;
    let player_spawn_y = -0.9;

    player.position[0] = player_spawn_x;
    player.position[1] = player_spawn_y;
    
    let mut vaos: Vec<u32> = Vec::new();
    let mut vbos: Vec<u32> = Vec::new();
    
    // Set vao and vbo for player
    initialize_object_vos(&player.vertices, true, &mut vaos, &mut vbos);
    
    //Player Projectile
    let mut projectile_fired: bool = false;

    let mut player_projectile = Circle::new([player.position[0], player.position[1]], 30.0);
    initialize_object_vos(&player_projectile.vertices, false, &mut vaos, &mut vbos);

    // Stage 1 ///////////////////////////////////

    // Horizontal platform
    let rectangle_1: Quad = Quad::new([400.0,620.0], 880.0, 20.0); //[horizontal position,vertical position], width, height
    initialize_object_vos(&rectangle_1.vertices, false, &mut vaos, &mut vbos); // vo 2

    // Vertical wall
    let rectangle_2: Quad = Quad::new([400.0,630.0], 20.0, 200.0);
    initialize_object_vos(&rectangle_2.vertices, false, &mut vaos, &mut vbos); // vo 3

    let mut enemy_dead: bool = false;
    let enemy: Triangle = create_enemy([950.0, 605.0], 30.0);
    initialize_object_vos(&enemy.vertices, false, &mut vaos, &mut vbos); // vo 4

    let mut circle_projectile: Circle = Circle::new([enemy.position[0],enemy.position[1]], 20.0);
    initialize_object_vos(&circle_projectile.vertices, false, &mut vaos, &mut vbos); // vo 5

    let stage_1_collision_bounds: Vec<[f32;4]> = Vec::from([rectangle_1.edges, rectangle_2.edges]);
    let stage_1_background: [f32;4] = [135.0 / 255.0, 206.0 / 255.0, 235.0 / 255.0, 100.0 / 100.0];

    /////////////////////////////////// Stage 1 //

    // Stage 2 ///////////////////////////////////

    let platform1: Quad = Quad::new([300.0,600.0], 300.0, 20.0); //[horizontal position,vertical position], width, height
    initialize_object_vos(&platform1.vertices, false, &mut vaos, &mut vbos); // vo 6

    let platform2: Quad = Quad::new([700.0,400.0], 300.0, 20.0);
    initialize_object_vos(&platform2.vertices, false, &mut vaos, &mut vbos); // vo 7

    let platform3: Quad = Quad::new([1100.0,200.0], 300.0, 20.0);
    initialize_object_vos(&platform3.vertices, false, &mut vaos, &mut vbos); // vo 8

    let wall1: Quad = Quad::new([1100.0,200.0], 20.0, 520.0);
    initialize_object_vos(&wall1.vertices, false, &mut vaos, &mut vbos); // vo 9

    let enemy2: Triangle = create_enemy([1000.0, 705.0], 30.0);
    initialize_object_vos(&enemy2.vertices, false, &mut vaos, &mut vbos); // vo 10

    let mut circle2_projectile = Circle::new([enemy2.position[0],enemy2.position[1]], 20.0);
    initialize_object_vos(&circle2_projectile.vertices, false, &mut vaos, &mut vbos); // vo 11

    let stage_2_collision_bounds: Vec<[f32;4]> = Vec::from([platform1.edges, platform2.edges, platform3.edges, wall1.edges]);

    //////////////////////////////////// Stage 2 //


    // Stage 3 ///////////////////////////////////
    
    let ceiling: Quad = Quad::new([700.0,100.0], 300.0, 20.0); //[horizontal position,vertical position], width, height
    initialize_object_vos(&ceiling.vertices, false, &mut vaos, &mut vbos); // vo 12

    let platform4: Quad = Quad::new([800.0,200.0], 300.0, 20.0);
    initialize_object_vos(&platform4.vertices, false, &mut vaos, &mut vbos); // vo 13

    let wall2: Quad = Quad::new([800.0,200.0], 20.0, 520.0);
    initialize_object_vos(&wall2.vertices, false, &mut vaos, &mut vbos); // vo 14

    let wall3: Quad = Quad::new([1200.0,0.0], 20.0, 600.0);
    initialize_object_vos(&wall3.vertices, false, &mut vaos, &mut vbos); // vo 15

    let enemy3: Triangle = create_enemy([850.0, 705.0], 30.0);
    initialize_object_vos(&enemy3.vertices, false, &mut vaos, &mut vbos); // vo 16

    let mut circle3_projectile = Circle::new([enemy3.position[0],enemy3.position[1]], 20.0);
    initialize_object_vos(&circle3_projectile.vertices, false, &mut vaos, &mut vbos); // vo 17

    let stage_3_collision_bounds: Vec<[f32;4]> = Vec::from([ceiling.edges, wall2.edges, platform4.edges, wall3.edges]);

    /////////////////////////////////// Stage 3 //


    // Death Stage ///////////////////////////////

    // draw YOU DIED
    let y_1: Quad = Quad::new([250.0,100.0], 20.0, 90.0);
    initialize_object_vos(&y_1.vertices, false, &mut vaos, &mut vbos); // vo 18
    let y_2: Quad = Quad::new([250.0,190.0], 200.0, 20.0);
    initialize_object_vos(&y_2.vertices, false, &mut vaos, &mut vbos); // vo 19
    let y_3: Quad = Quad::new([430.0,100.0], 20.0, 90.0);
    initialize_object_vos(&y_3.vertices, false, &mut vaos, &mut vbos); // vo 20
    let y_4: Quad = Quad::new([340.0,210.0], 20.0, 90.0);
    initialize_object_vos(&y_4.vertices, false, &mut vaos, &mut vbos); // vo 21

    let o_1: Quad = Quad::new([530.0,120.0], 20.0, 160.0);
    initialize_object_vos(&o_1.vertices, false, &mut vaos, &mut vbos); // vo 22
    let o_2: Quad = Quad::new([530.0,100.0], 200.0, 20.0);
    initialize_object_vos(&o_2.vertices, false, &mut vaos, &mut vbos); // vo 23
    let o_3: Quad = Quad::new([530.0,280.0], 200.0, 20.0);
    initialize_object_vos(&o_3.vertices, false, &mut vaos, &mut vbos); // vo 24
    let o_4: Quad = Quad::new([710.0,120.0], 20.0, 160.0);
    initialize_object_vos(&o_4.vertices, false, &mut vaos, &mut vbos); // vo 25

    let u_1: Quad = Quad::new([820.0,100.0], 20.0, 180.0);
    initialize_object_vos(&u_1.vertices, false, &mut vaos, &mut vbos); // vo 26
    let u_2: Quad = Quad::new([1000.0,100.0], 20.0, 180.0);
    initialize_object_vos(&u_2.vertices, false, &mut vaos, &mut vbos); // vo 27
    let u_3: Quad = Quad::new([820.0,280.0], 200.0, 20.0);
    initialize_object_vos(&u_3.vertices, false, &mut vaos, &mut vbos); // vo 28

    let d1_1: Quad = Quad::new([105.0,420.0], 20.0, 160.0);
    initialize_object_vos(&d1_1.vertices, false, &mut vaos, &mut vbos); // vo 29
    let d1_2: Quad = Quad::new([105.0,400.0], 160.0, 20.0);
    initialize_object_vos(&d1_2.vertices, false, &mut vaos, &mut vbos); // vo 30
    let d1_3: Quad = Quad::new([105.0,580.0], 160.0, 20.0);
    initialize_object_vos(&d1_3.vertices, false, &mut vaos, &mut vbos); // vo 31
    let d1_4: Quad = Quad::new([265.0,420.0], 20.0, 160.0);
    initialize_object_vos(&d1_4.vertices, false, &mut vaos, &mut vbos); // vo 32

    
    let i_1: Quad = Quad::new([395.0,400.0], 200.0, 20.0);
    initialize_object_vos(&i_1.vertices, false, &mut vaos, &mut vbos); // vo 33
    let i_2: Quad = Quad::new([485.0,420.0], 20.0, 160.0);
    initialize_object_vos(&i_2.vertices, false, &mut vaos, &mut vbos); // vo 34
    let i_3: Quad = Quad::new([395.0,580.0], 200.0, 20.0);
    initialize_object_vos(&i_3.vertices, false, &mut vaos, &mut vbos); // vo 35
    
    let e_1: Quad = Quad::new([685.0,400.0], 20.0, 200.0);
    initialize_object_vos(&e_1.vertices, false, &mut vaos, &mut vbos); // vo 36
    let e_2: Quad = Quad::new([705.0,400.0], 180.0, 20.0);
    initialize_object_vos(&e_2.vertices, false, &mut vaos, &mut vbos); // vo 37
    let e_3: Quad = Quad::new([705.0,580.0], 180.0, 20.0);
    initialize_object_vos(&e_3.vertices, false, &mut vaos, &mut vbos); // vo 38
    let e_4: Quad = Quad::new([705.0,490.0], 100.0, 20.0);
    initialize_object_vos(&e_4.vertices, false, &mut vaos, &mut vbos); // vo 39
    
    let d2_1: Quad = Quad::new([975.0,420.0], 20.0, 160.0);
    initialize_object_vos(&d2_1.vertices, false, &mut vaos, &mut vbos); // vo 40
    let d2_2: Quad = Quad::new([975.0,400.0], 160.0, 20.0);
    initialize_object_vos(&d2_2.vertices, false, &mut vaos, &mut vbos); // vo 41
    let d2_3: Quad = Quad::new([975.0,580.0], 160.0, 20.0);
    initialize_object_vos(&d2_3.vertices, false, &mut vaos, &mut vbos); // vo 42
    let d2_4: Quad = Quad::new([1135.0,420.0], 20.0, 160.0);
    initialize_object_vos(&d2_4.vertices, false, &mut vaos, &mut vbos); // vo 43

    let stage_death_collision_bounds: Vec<[f32;4]> = Vec::from([
        y_1.edges, y_2.edges, y_3.edges, y_4.edges,
        o_1.edges, o_2.edges, o_3.edges, o_4.edges,
        u_1.edges, u_2.edges, u_3.edges,
        d1_1.edges, d1_2.edges, d1_3.edges, d1_4.edges,
        i_1.edges, i_2.edges, i_3.edges,
        e_1.edges, e_2.edges, e_3.edges, e_4.edges,
        d2_1.edges, d2_2.edges, d2_3.edges, d2_4.edges]);
    let death_stage_background: [f32;4] = [178.0 / 255.0, 34.0 / 255.0, 34.0 / 255.0, 100.0 / 100.0];

    //////////////////////////////// Death Stage //
     

    // Stage 4 ////////////////////////////////////



    //////////////////////////////////// Stage 4 //
    
    
    
    // Stage 5 ////////////////////////////////////



    //////////////////////////////////// Stage 5 //
    

    // Write a simple vertex shader
    let vertex_shader_src = CString::new(
        r#"
        #version 330 core
        layout (location = 0) in vec2 aPos;

        uniform mat4 model;

        void main() {
            gl_Position = model * vec4(aPos, 0.0, 1.0);
        }
    "#,
    ).unwrap();

    // Sets color for player
    let player_shader_program = create_shader_program(vertex_shader_src.clone(), 255.0, 128.0, 51.0, 1.0);
    let player_location = unsafe { gl::GetUniformLocation(player_shader_program, CString::new("model").unwrap().as_ptr()) };
    
    // Sets color for walls/platforms
    let death_stage_wall_shader_program = create_shader_program(vertex_shader_src.clone(), 20.0, 20.0, 20.0, 1.0);
    let death_stage_location = unsafe { gl::GetUniformLocation(death_stage_wall_shader_program, CString::new("model").unwrap().as_ptr()) };

    let stage_1_wall_shader_program = create_shader_program(vertex_shader_src.clone(), 211.0, 211.0, 211.0, 1.0);
    let stage_1_location = unsafe { gl::GetUniformLocation(stage_1_wall_shader_program, CString::new("model").unwrap().as_ptr()) };

    let stage_2_wall_shader_program = create_shader_program(vertex_shader_src.clone(), 211.0, 111.0, 218.0, 1.0);
    let stage_2_location = unsafe { gl::GetUniformLocation(stage_2_wall_shader_program, CString::new("model").unwrap().as_ptr()) };

    let stage_3_wall_shader_program = create_shader_program(vertex_shader_src.clone(), 211.0, 111.0, 225.0, 1.0);
    let stage_3_location = unsafe { gl::GetUniformLocation(stage_3_wall_shader_program, CString::new("model").unwrap().as_ptr()) };

    // Sets color for enemies
    let enemy_shader_program = create_shader_program(vertex_shader_src.clone(), 255.0, 62.0, 62.0, 1.0);

    // Sets color for enemies
    let enemy2_shader_program = create_shader_program(vertex_shader_src.clone(), 255.0, 31.0, 31.0, 1.0);

    let enemy3_shader_program = create_shader_program(vertex_shader_src.clone(), 255.0, 15.0, 15.0, 1.0);

    // Get the location of the 'model' uniform
    let enemy_model_location = unsafe { gl::GetUniformLocation(enemy_shader_program, CString::new("model").unwrap().as_ptr()) };
    let enemy2_model_location = unsafe { gl::GetUniformLocation(enemy2_shader_program, CString::new("model").unwrap().as_ptr()) };
    let enemy3_model_location = unsafe { gl::GetUniformLocation(enemy3_shader_program, CString::new("model").unwrap().as_ptr()) };
    
    

    // Mandatory wall location variables
    let default_location: [f32; 2] = [0.0,0.0]; 
    let default_location_model = glm::translate(&glm::identity(), &glm::vec3(default_location[0], default_location[1], 0.0));
    let default_location_model_ptr: *const f32 = default_location_model.as_ptr() as *const f32;
    
    let x_acceleration: f32 = 4.1;
    let friction: f32 = 1.9;
    let max_speed: f32 = 0.4;
    let jump_velocity: f32 = 3.0;
    let gravity: f32 = 7.0;
    let mut player_x_velocity: f32 = 0.0;
    let mut player_y_velocity: f32 = 0.0;

    let mut last_time = Instant::now();

    let mut pressed_keys = HashSet::new(); // Track the pressed keys

    let mut jumping: bool = false;
    
    let mut mouse_position: [f64; 2] = [0.0, 0.0];

    let mut player_projectile_direction: [f32;2] = [0.0,0.0];

    let mut player_projectile_cooldown: f32 = 0.0;
    
    // Run the event loop
    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent { event, .. } => match event {
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            WindowEvent::KeyboardInput { input, .. } => {
                if let Some(keycode) = input.virtual_keycode {
                    match input.state {
                        ElementState::Pressed => {
                            pressed_keys.insert(keycode); // Store the pressed key
                        }
                        ElementState::Released => {
                            pressed_keys.remove(&keycode); // Remove the released key
                        }
                    }
                }
            }
            WindowEvent::CursorMoved { position, .. } => {
                mouse_position = [position.x * monitor_width_ratio as f64, position.y * monitor_height_ratio as f64]
                //println!("Mouse position x = {}, Mouse position y = {}", position.x * monitor_width_ratio as f64, position.y * monitor_height_ratio as f64);
            }
            WindowEvent::MouseInput { button, .. } => {
                if button == glutin::event::MouseButton::Left {
                    println!("Mouse position x = {}, Mouse position y = {}", mouse_position[0], mouse_position[1])
                }
            }
            _ => (),
        },
        Event::MainEventsCleared => {
            let now = Instant::now();
            let delta_time = now - last_time;

            let delta_seconds = delta_time.as_secs_f32();

            player_projectile_cooldown -= delta_seconds;

            // Moves enemy 1 projectile to the left
            if !enemy_dead {    
                if circle_projectile.position[0] > -1.0 {
                    circle_projectile.position[0] -= delta_seconds * 0.75;
                } else {
                    circle_projectile.position[0] = enemy.position[0];
                }
                // Moves enemy 2 projectile to the left // WAS RIGHT
                if circle2_projectile.position[0] > -1.0 { // WAS < 1.0
                    circle2_projectile.position[0] -= delta_seconds * 0.75; // WAS +=
                } else {
                    circle2_projectile.position[0] = enemy2.position[0];
                }

                if circle3_projectile.position[0] < 1.0 { // WAS < 1.0
                    circle3_projectile.position[0] += delta_seconds * 0.75; // WAS +=
                } else {
                    circle3_projectile.position[0] = enemy3.position[0];
                }
            }
            if enemy_dead {
                circle_projectile.position[0] = -5000.0;
                circle_projectile.position[1] = -5000.0;
                circle2_projectile.position[0] = -5000.0;
                circle2_projectile.position[1] = -5000.0;
                circle3_projectile.position[0] = -5000.0;
                circle3_projectile.position[1] = -5000.0;
                
            }

            // Control player
            for keycode in &pressed_keys {
                match keycode {
                    VirtualKeyCode::LControl => {
                        gl_window.window().set_fullscreen(Some(glutin::window::Fullscreen::Borderless(None)));
                    }
                    VirtualKeyCode::RControl => {
                        gl_window.window().set_fullscreen(None);
                        gl_window.window().set_maximized(true);
                    }
                    VirtualKeyCode::W => {
                        if !jumping {
                            player_y_velocity += jump_velocity;
                            jumping = true;
                        }
                    }
                    VirtualKeyCode::A => {
                        player_x_velocity -= x_acceleration * delta_seconds;
                    }
                    VirtualKeyCode::D => {
                        player_x_velocity += x_acceleration * delta_seconds;
                    }
                    VirtualKeyCode::Space => {
                        if !jumping {
                            player_y_velocity += jump_velocity;
                            jumping = true;
                        }
                    }
                    VirtualKeyCode::E => {
                        if !projectile_fired && player_projectile_cooldown <= 0.0 {
                            projectile_fired = true;
                            player_projectile.position[0] = player.position[0];
                            player_projectile.position[1] = player.position[1];
                            let logical_mouse_position: [f32; 2] = pixel_position_to_logical_position(mouse_position[0] as f32, mouse_position[1] as f32) ;
                            player_projectile_direction = get_unit_vector([logical_mouse_position[0] - player.position[0], logical_mouse_position[1] - player.position[1]]);
                        }
                    }
                    _ => (),
                }
            }
            
            let mut bounds: &Vec<[f32; 4]> = &Vec::new();
            let mut player_hit: bool = false;
            match game_stage {
                0 => {
                    unsafe { gl::ClearColor(stage_1_background[0], stage_1_background[1], stage_1_background[2], stage_1_background[3]) }
                    bounds = &stage_1_collision_bounds;
                    if (player.position[0] - circle_projectile.position[0]).abs() < player.edge_offsets[0] && (player.position[1] - circle_projectile.position[1]).abs() < player.edge_offsets[1]
                    { player_hit = true; }
                    if (enemy.position[0] - player_projectile.position[0]).abs() < enemy.edge_offsets[0] && (enemy.position[1] - player_projectile.position[1]).abs() < enemy.edge_offsets[1]
                    { enemy_dead = true; }
                                        
                },
                1 => {
                    unsafe { gl::ClearColor(stage_1_background[0], stage_1_background[1], stage_1_background[2], stage_1_background[3]) }
                    bounds = &stage_2_collision_bounds;
                    if (player.position[0] - circle2_projectile.position[0]).abs() < player.edge_offsets[0] && (player.position[1] - circle2_projectile.position[1]).abs() < player.edge_offsets[1]
                    { player_hit = true; }
                    if (enemy2.position[0] - player_projectile.position[0]).abs() < enemy2.edge_offsets[0] && (enemy2.position[1] - player_projectile.position[1]).abs() < enemy2.edge_offsets[1]
                    { enemy_dead = true; }
                },
                2 => {
                    unsafe { gl::ClearColor(stage_1_background[0], stage_1_background[1], stage_1_background[2], stage_1_background[3]) }
                    bounds = &stage_3_collision_bounds;
                    if (player.position[0] - circle3_projectile.position[0]).abs() < player.edge_offsets[0] && (player.position[1] - circle3_projectile.position[1]).abs() < player.edge_offsets[1]
                    { player_hit = true; }
                    if (enemy3.position[0] - player_projectile.position[0]).abs() < enemy3.edge_offsets[0] && (enemy3.position[1] - player_projectile.position[1]).abs() < enemy3.edge_offsets[1]
                    { enemy_dead = true; }
                },
                -1 => {
                    unsafe {gl::ClearColor(death_stage_background[0], death_stage_background[1], death_stage_background[2], death_stage_background[3]) }
                    bounds = &stage_death_collision_bounds;
                },
                _ => println!("Uncaught Switch Case at game_stage {}", game_stage),
            }

            if player_hit {
                player.position[0] = player_spawn_x;
                player.position[1] = player_spawn_y;

                player_x_velocity = 0.0;
                player_y_velocity = 0.0;

                game_stage = -1;
            }

            
            handle_player_movement(&mut player, delta_seconds, &mut player_x_velocity, friction, max_speed, &mut player_y_velocity, gravity, &mut jumping, &bounds);
            
            // Player projectile movement
            if projectile_fired {
                let collided: bool = handle_projectile_movement(&mut player_projectile, &player_projectile_direction, 2.0, delta_seconds, &bounds);
                if collided {
                    player.position[0] = player_projectile.position[0]; 
                    player.position[1] = player_projectile.position[1]; 
                    projectile_fired = false;
                    player_projectile_cooldown = 0.3; // Cooldown in seconds before teleport projectile can be fired again
                }
            }

            // Switches to next stage if player reaches the right side of the screen
            if game_stage < max_stage && player.position[0] + player.edge_offsets[0] >= 1.0 && enemy_dead {
                game_stage += 1;
                enemy_dead = false;
                circle2_projectile.position[0] = enemy2.position[0];
                circle2_projectile.position[1] = enemy2.position[1];
                circle3_projectile.position[0] = enemy3.position[0];
                circle3_projectile.position[1] = enemy3.position[1];
                player.position[0] = -0.99 + player.edge_offsets[0];
            }
            // Switches to previous stage if player reaches the left side of the screen
            else if game_stage > 0 && player.position[0] - player.edge_offsets[0] <= -1.0 {
                game_stage -= 1;
                player.position[0] = 0.99 - player.edge_offsets[0];
            }
            else if game_stage == -1 && player.position[0] + player.edge_offsets[0] >= 1.0 {
                game_stage += 1;
                player.position[0] = -0.99 + player.edge_offsets[0];
            }
            
            
            last_time = now;
            gl_window.window().request_redraw();
        }
        Event::RedrawRequested(_) => {
            // Clear the screen
            unsafe {
                gl::Clear(gl::COLOR_BUFFER_BIT);
            }

            // Draw the player
            let model = glm::translate(&glm::identity(), &glm::vec3(player.position[0], player.position[1], 0.0));
            let player_ptr: *const f32 = model.as_ptr() as *const f32;
            unsafe {
                gl::UseProgram(player_shader_program);
                gl::UniformMatrix4fv(player_location, 1, gl::FALSE, player_ptr);
                gl::BindVertexArray(vaos[0]);
                draw_triangle();
            }

            // Draw player's projectile
            if projectile_fired {
                let player_circle_model = glm::translate(&glm::identity(), &glm::vec3(player_projectile.position[0], player_projectile.position[1], 0.0));
                let player_circle_model_ptr: *const f32 = player_circle_model.as_ptr() as *const f32;
                unsafe {
                    gl::UseProgram(stage_1_wall_shader_program);
                    gl::UniformMatrix4fv(stage_1_location, 1, gl::FALSE, player_circle_model_ptr);
                    gl::BindVertexArray(vaos[1]);
                    draw_circle();
                }
            }

            // Using a switch case to change the game stage when character moves to the left / right of the screen

            match game_stage {
                // First game stage
                0 => {
                    // Set the background color
                    unsafe {
                        
                        // Draw walls
                        gl::UseProgram(stage_1_wall_shader_program);
                        gl::UniformMatrix4fv(stage_1_location, 1, gl::FALSE, default_location_model_ptr);
                        for i in 2..4 {
                            gl::BindVertexArray(vaos[i]);
                            draw_rectangle();
                        }
                    }
                    
                    if !enemy_dead {    
                        // Draw enemy's projectile
                        let enemy_circle_model = glm::translate(&glm::identity(), &glm::vec3(circle_projectile.position[0], circle_projectile.position[1], 0.0));
                        let enemy_circle_model_ptr: *const f32 = enemy_circle_model.as_ptr() as *const f32;
                        unsafe {
                            gl::UseProgram(stage_2_wall_shader_program);
                            gl::UniformMatrix4fv(stage_1_location, 1, gl::FALSE, enemy_circle_model_ptr);
                            gl::BindVertexArray(vaos[5]);
                            draw_circle();
                        }
                        // Draw an enemy
                        let enemy_model = glm::translate(&glm::identity(), &glm::vec3(enemy.position[0], enemy.position[1], 0.0));
                        let enemy_model_ptr: *const f32 = enemy_model.as_ptr() as *const f32;
                        unsafe {
                            gl::UseProgram(enemy_shader_program);
                            gl::UniformMatrix4fv(enemy_model_location, 1, gl::FALSE, enemy_model_ptr);
                            gl::BindVertexArray(vaos[4]);
                            draw_triangle();
                        }
                    }
                    

                }
                // Second game stage
                1 => {
                    // Set the background color
                    unsafe {
                        gl::ClearColor(stage_1_background[0], stage_1_background[1], stage_1_background[2], stage_1_background[3]);
                        
                        // Draw platforms and walls

                        gl::UseProgram(stage_1_wall_shader_program);
                        gl::UniformMatrix4fv(stage_2_location, 1, gl::FALSE, default_location_model_ptr);
                        for i in 6..10 {
                            gl::BindVertexArray(vaos[i]);
                            draw_rectangle();
                        }
                    }
                    
                    if !enemy_dead {    
                        // Draw enemy 2's projectile
                        let circle2_model = glm::translate(&glm::identity(), &glm::vec3(circle2_projectile.position[0], circle2_projectile.position[1], 0.0));
                        let circle2_model_ptr: *const f32 = circle2_model.as_ptr() as *const f32;
                        unsafe {
                            gl::UseProgram(stage_2_wall_shader_program);
                            gl::UniformMatrix4fv(stage_2_location, 1, gl::FALSE, circle2_model_ptr);
                            gl::BindVertexArray(vaos[11]);
                            draw_circle();
                        }

                        // Draw enemy 2
                        let enemy2_model = glm::translate(&glm::identity(), &glm::vec3(enemy2.position[0], enemy2.position[1], 0.0));
                        let enemy2_model_ptr: *const f32 = enemy2_model.as_ptr() as *const f32;
                        unsafe {
                            gl::UseProgram(enemy2_shader_program);
                            gl::UniformMatrix4fv(enemy2_model_location, 1, gl::FALSE, enemy2_model_ptr);
                            gl::BindVertexArray(vaos[10]);
                            draw_triangle();
                        }
                    }
                    
                }
                // Third game stage
                2 => {
                    // Set the background color
                    unsafe {
                        gl::ClearColor(stage_1_background[0], stage_1_background[1], stage_1_background[2], stage_1_background[3]);
                        
                        // Draw platforms and walls
                        gl::UseProgram(stage_1_wall_shader_program);
                        gl::UniformMatrix4fv(stage_3_location, 1, gl::FALSE, default_location_model_ptr);
                        for i in 12..16 {
                            gl::BindVertexArray(vaos[i]);
                            draw_rectangle();
                        }
                    }

                    if !enemy_dead {
                        // Draw enemy 2's projectile
                        let circle3_model = glm::translate(&glm::identity(), &glm::vec3(circle3_projectile.position[0], circle3_projectile.position[1], 0.0));
                        let circle3_model_ptr: *const f32 = circle3_model.as_ptr() as *const f32;
                        unsafe {
                            gl::UseProgram(stage_2_wall_shader_program);
                            gl::UniformMatrix4fv(stage_3_location, 1, gl::FALSE, circle3_model_ptr);
                            gl::BindVertexArray(vaos[17]);
                            draw_circle();
                        }
                        
                        // Draw enemy 2
                        let enemy3_model = glm::translate(&glm::identity(), &glm::vec3(enemy3.position[0], enemy3.position[1], 0.0));
                        let enemy3_model_ptr: *const f32 = enemy3_model.as_ptr() as *const f32;
                        unsafe {
                            gl::UseProgram(enemy3_shader_program);
                            gl::UniformMatrix4fv(enemy3_model_location, 1, gl::FALSE, enemy3_model_ptr);
                            gl::BindVertexArray(vaos[16]);
                            draw_triangle();
                        }
                    }
                    
                }
                // Player death game stage
                -1 => {
                    // Set the background color
                    unsafe {
                        // Draw walls
                        gl::UseProgram(death_stage_wall_shader_program);
                        gl::UniformMatrix4fv(death_stage_location, 1, gl::FALSE, default_location_model_ptr);
                        for i in 18..44 {
                            gl::BindVertexArray(vaos[i]);
                            draw_rectangle();
                        }
                    }
                }
                // Catch exceptions
                _ => println!("Uncaught Switch Case at game_stage {}", game_stage),
            }

            // Swap buffers
            gl_window.swap_buffers().unwrap();
        }        
        _ => (),
    });
}
